This is the website source created for https://recordswap.com. 

# Configuring

Edit `_data/business.yml` and `_data/website.yml` to update information like the phone number, address, site banner, etc. They're fairly customizable.

Most longform text is baked into the HTML, but some pieces are kept neatly separate inside of `.md` files.

# Building

This site is designed to be built with [Jekyll](https://jekyllrb.com/) 3.8. Develop with `jekyll serve --livereload` and build with `jekyll build`.